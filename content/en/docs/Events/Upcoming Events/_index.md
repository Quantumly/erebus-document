---
title: "Upcoming Events"
linkTitle: "Upcoming Events"
weight: 1
description: >
  List of related events that are scheduled to be held in the future
---

## RoboCupJunior 2021 International Competition

The Erebus simulator will be used for participants competiting in the RoboCupJunior Rescue New Simulation (Demonstration) competition. 

<!--
|Action|Deadline|
|------|--------|
|Competition Date, will be streamed online| tbd|
|Deadline for submitting your programs|	tbd|
|Completion of registration of interest| tbd|
-->

For more information, please visit the [community webpage](https://rescue.rcj.cloud/events/2021/robocup2021/) for the 2021 RCJ Rescue competition. 
