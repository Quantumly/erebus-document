---
title: "Past Events"
linkTitle: "Past Events"
weight: 2
description: >
  List of related events that were held in the past
---

## October 2020 International Online Demonstration Competition

We hosted the first international, online competition in October 2020. Teams worked on their programs, and then submitted these to be tested against other around the world on an unseen map.

<!--
|Action|Deadline|
|------|--------|
|Competition Date, will be streamed online|10/04/20 12:00:00 UTC|
|Deadline for submitting your programs|	09/25/20 23:59:59 UTC|
|Completion of registration of interest| 	09/18/20 23:59:59 UTC|
-->

For more information, please visit the [official page](https://rescue.rcj.cloud/events/2020/simulation/index.html) of the demonstration competition.

> *Include link to YouTube livestream*